FROM python:latest

RUN apt update && \
    apt upgrade -y && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app
COPY . .

RUN python -m pip install --upgrade pip

RUN pip install --no-cache-dir -r requirements.txt

CMD ["python", "manage.py", "runserver_plus", "--nopin", "0.0.0.0:8000"]
