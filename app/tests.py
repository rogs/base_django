"""Mock test to test pytest."""


def inc(number: int) -> int:
    """Mock test to test pytest."""
    return number + 1


def test_answer() -> None:
    """Mock test to test pytest."""
    assert inc(3) == 4
