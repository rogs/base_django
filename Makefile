build:
	docker-compose build --pull

makemigrations:
	docker-compose run --rm app python manage.py makemigrations

migrate:
	docker-compose run --rm app python manage.py migrate

createsu:
	docker-compose run --rm app python manage.py createsuperuser

init: build migrate

# Development

up: migrate
	docker-compose up

django-manage:
	docker-compose run app python manage.py $(command)

shell:
	docker-compose run app python manage.py shell_plus --bpython

full: init up

kill:
	docker-compose down

cleanup:
	docker-compose down -v

test:
	docker-compose run --rm app sh -c 'python -m pytest -k "$(target)" --exitfirst'

# Utils

bash:
	docker-compose run --rm app bash
