# Django Base App

This is my Django starter base app. Nothing too fancy.

## What does it includes?
- Django
- Django REST Framework
- django-extensions
- drf-yasg for automatic swagger
- Docker & docker-compose with a Postgres DB
- GitlabCI / Github Actions
- Linting with:
    + black
    + isort
    + flake8 
    + pylint
    + mypy
    + pytest
- pre-commit configuration
- Makefile
- More?

## How do I use this?

Easy.

First, clone the repo:
``` sh
git clone git@gitlab.com:rogs/base_django.git --depth 1 <my_project_name>
cd <my_project_name>
```

Then, remove the Git folder, create a new git repo and add the remote:

``` sh
rm -rf .git
git init
git remote add origin <my_awesome_git_repo>
```

Finally, install the virtualenv:

``` sh
virtualenv .venv
source .venv/bin/activate
(.venv) pip install -r requirements.txt
```

## Available commands

The Makefile simplifies common development tasks for Django projects using Docker and Docker Compose. It provides shortcuts to build images, run migrations, start the development environment, and more.

### Base

- `build:`

Build Docker images using `docker-compose`.

- `makemigrations:`

Create new database migrations for your Django project.

- `migrate:`

Run database migrations to apply changes to the database schema.

- `createsu:`

Create a superuser for your Django application.

- `init:`

Combine `build` and `migrate` to set up the development environment.

### Development:

- `up:`

Start the development environment with `docker-compose`.

- `django-manage:`

A flexible target for running Django management commands. Use it like this:

    make django-manage command=<Django_Command>

For example, to run `python manage.py test`, you would use:

    make django-manage command=test

- `shell:`

Start a Django shell using `shell_plus`. This target provides an interactive Python environment for your project.

- `full:`

A combination of `init` and `up`. Useful for quickly setting up and starting the development environment.

- `kill:`

Stop and remove containers started with `docker-compose`.

- `cleanup:`

This target cleans up containers and removes associated volumes.

- `test:`

Use this target to run your Django tests. It accepts a `target` parameter to specify which tests to run.

    make test target=<test_target>

Replace `<test_target>` with the specific tests you want to run.

- `bash:`

This target starts a Bash shell inside the Django app container, allowing you to access the container's shell.
